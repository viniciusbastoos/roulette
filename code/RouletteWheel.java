import java.util.Random;

public class RouletteWheel{
    private Random random;

    public RouletteWheel() {
        random = new Random();
    }
    
    private int numLastSpin = 0;

    public void spin(){
        numLastSpin = random.nextInt(37);

        System.out.println("The number was: " + numLastSpin);
    }

    public int getValue(){
        return numLastSpin;
    }
}
