import java.util.Scanner;

public class Roulette{
    public static void main(String[] args) {
        Scanner scan = new Scanner(System.in);

        boolean main1 = true;
        boolean main2 = true;

        // 1.
        RouletteWheel RouletteWheel = new RouletteWheel();

        // 2.
        int totalMoney = 1000;

        // 3.
       
        while (main1){
            System.out.println("Would you like to make a bet? 1-YES or 2-NO");
            String input1 = scan.nextLine();  
        
            try{ 
                int input101 = Integer.parseInt(input1);  // if its not an int, goes to catch

                if(input101 == 1){

                    if(totalMoney == 0){
                        System.out.println("You don't have enough money to play. Goodbye!");
                        main1 = false;
                        main2 = false;
                    }
                    else{

                        System.out.println("How much would you like to bet?");
                        String input2 = scan.nextLine(); 

                        try{
                            int input202 = Integer.parseInt(input2); // if its not an int, goes to catch

                            if(input202 > totalMoney){
                                System.out.println("You don't have enough money. You have: " + totalMoney);
                            }

                            // 4.    
                            else{
                                int betNumber;

                                while (true){
                                    System.out.print("Enter the number to bet on (0-36): ");
                                    betNumber = Integer.parseInt(scan.nextLine());
                    
                                    if (betNumber >= 0 && betNumber <= 36){ // cannot write anything but numbers from 0-36
                                        break;
                                    } 
                                    else{
                                        System.out.println("Invalid number. Please enter a number between 0 and 36.");
                                    }
                                }
                    
                                RouletteWheel.spin();
                                int getValue = RouletteWheel.getValue();

                                if (getValue == betNumber){
                                    int winnings = input202 * 35; 
                                    totalMoney += winnings;

                                    System.out.println("Congratulations! You won $" + winnings);
                                } 
                                else{
                                    totalMoney -= input202;

                                    System.out.println("Sorry, you lost $" + input202 + ". You now have: $" + totalMoney);
                                }
                            }

                        }                        
                        catch (NumberFormatException e){
                            System.out.println("What you wrote is not a valid integer. Please, repeat!"); 
                        }
                    }   
                }
                else if (input101 == 2){  
                    System.out.println("END OF GAME!");
                    
                    if(totalMoney < 1000){
                        int amountLost = 1000-totalMoney;
                        System.out.println("You lost: $" + amountLost);
                    }
                    else{
                        int amountWon  = totalMoney-1000;
                        System.out.println("You won: $" + amountWon);
                    }

                    main1 = false;
                }
                else{
                    System.out.println("This number is incorrect. Either 1 or 2, please!");
                }
            }  
            catch (NumberFormatException e){ 
                System.out.println(input1 + " is not a valid integer. Please, repeat"); 
            } 
        }
    }
}
